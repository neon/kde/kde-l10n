#!/bin/bash

set -e

mode="upstream"
component=""
version=""

while [[ $# -ge 1 ]]; do
    case "$1" in
        --upstream)
            shift
            mode="upstream"
            ;;
        --local)
            shift
            mode="local"
            ;;
        --diff)
            shift
            mode="diff"
            component="yes"
            ;;
        --component)
            shift
            component="yes"
            ;;
        *)
            break
            ;;
    esac
done

check_version() {
    if [[ $# -lt 1 ]]; then
        echo "Missing upstream version" >&2
        echo "Use: $0 VERSION" >&2
        exit 1
    fi
}

upstream () {
    version="$1"

    lftp -c 'open https://download.kde.org/stable/applications/'"$version"'/src/kde-l10n/; ls' | \
        sed -n '/^.*kde-l10n-\(.*\)-'"$version"'\.tar\.xz$/s//\1/p' | \
        if [[ -z "$component" ]]; then
            cat
        else
            sed 's/[_@]//g' | tr '[:upper:]' '[:lower:]'
        fi
}

local_list () {
    dh_listpackages | sed 's/kde-l10n-//'
}

case "$mode" in
    upstream)
        check_version "$@"
        upstream "$@"
        ;;
    local)
        local_list
        ;;
    diff)
        check_version "$@"
        diff -u <(local_list) <(upstream "$@")
        ;;
esac
